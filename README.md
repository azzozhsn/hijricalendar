# HijriCalendar #

PHP class to generate html code to show monthly view for HijriCalendar.

### How do use it? ###

//initialize with data:

$calendar = new HijriCalendar();

$calendar->addEvent('1437/05/12', 1, 'موعد');

$calendar->addEvent('1437/05/15', 12, 'مشروع ١');

$calendar->addEvent('1437/05/20', 5, 'مشروع ٢');

$calendar->addEvent('1437/05/27', 3, 'إجازة');


// and where to output use:

$calendar->render();



see the example in index.php