<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
date_default_timezone_set('Asia/Riyadh');
require_once 'libs/HijriCalendar.class.php';

$calendar = new HijriCalendar();
$calendar->addEvent('1437/05/12', 1, 'موعد');
$calendar->addEvent('1437/05/15', 12, 'مشروع ١');
$calendar->addEvent('1437/05/20', 5, 'مشروع ٢');
$calendar->addEvent('1437/05/27', 3, 'إجازة');
?>

<!DOCTYPE html>
<html dir="rtl">
<head>
    <meta charset="utf-8">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>HijriCalendar Demo</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container" id="content">
    <div class="panel panel-default">
        <?= $calendar->render() ?>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
</body>
</html>