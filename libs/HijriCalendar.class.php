<?php
/**
 * ----------------------------------------------------------------------
 *
 * Copyright (c) 2016 Azzoz Al-Hasani.
 *
 * ----------------------------------------------------------------------
 *
 * LICENSE
 *
 * This program is open source product; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License (LGPL)
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.txt>.
 *
 * ----------------------------------------------------------------------
 *
 * Class Name: HijriCalendar
 *
 * Filename:   HijriCalendar.class.php
 *
 * Original    Author(s): Azzoz Al-Hasani <azzozhsn@gmail.com>
 *
 * Purpose:    HijriCalendar
 *
 * ----------------------------------------------------------------------
 *
 * HijriCalendar
 *
 * PHP class to generate html code to show monthly view for HijriCalendar.
 *
 */

require_once 'I18N_Arabic_Date.class.php';
require_once 'I18N_Arabic_Mktime.class.php';

class HijriCalendar
{
    private $time;
    private $month;
    private $year;
    private $events = [];

    private $ardate;
    private $mktime;

    /**
     * Loads initialize values
     *
     * @ignore
     */
    function __construct()
    {
        /** initiate ArabicDate object. It will be used to calculate hijri date */
        $this->ardate = new I18N_Arabic_Date();
        $this->ardate->setMode(1);
        /** initiate ArabicMkTime object. It will be used to get timestamp from hijri date */
        $this->mktime = new I18N_Arabic_Mktime();
        /** If not set yet, set the current month to be this month */
        if (!isset($_SESSION['HijriCalendarClassMonth'])) {
            $_SESSION['HijriCalendarClassMonth'] = $this->arDate('m');
            $_SESSION['HijriCalendarClassYear'] = $this->arDate('Y');
        }
        $this->month = $_SESSION['HijriCalendarClassMonth'];
        $this->year = $_SESSION['HijriCalendarClassYear'];
    }

    /**
     * Add event to HijriCalendar
     * @param $start string date for event's start
     * @param $duration int how many days the duration of event
     * @param $title string description for event.
     */
    public function addEvent($start, $duration, $title)
    {
        $this->events[] = ['start' => $start, 'duration' => $duration, 'title' => $title];
    }

    /**
     * Convert Western Arabic numerals to Eastern Arabic numerals
     * @param string $text text with numbers.
     * @return string converted text
     */
    function arNumbers($text)
    {
        return str_replace(['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'], ['١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩', '٠'], $text);
    }

    /**
     * Format a local time/date in Arabic string
     * @param string $format Format string (same as PHP date function)
     * @param int $time Unix timestamp
     * @return string Format Arabic date string according to given format string
     *                using the given integer timestamp or the current local
     *                time if no timestamp is given.
     */
    public function arDate($format, $time = 0)
    {
        $time = ($time > 0) ? $time : time();
        $diff = $this->ardate->dateCorrection($time);
        return($this->ardate->date($format, $time, $diff));
    }

    /**
     * This will return current Unix timestamp for given Hijri date (Islamic calendar)
     * @param int $day Hijri day
     * @param int $month Hijri month
     * @param int $year Hijri year
     * @return int
     */
    public function arMktime($day, $month=1, $year=1437)
    {
        if (strpos($day, '-'))
            list($y, $m, $d) = explode('-', $day);
        if (strpos($day, '/'))
            list($y, $m, $d) = explode('/', $day);
        $diff = $this->mktime->mktimeCorrection((int) $month, (int) $year);
        return($this->mktime->mktime(0, 0, 0, (int) $month, (int) $day, (int) $year, $diff));
    }

    /**
     * Generate a monthly view calendar as html code.
     * @return string the html code for the calendar.
     */
    public function render()
    {
        // previous button clicked
        if (isset($_POST['prev'])) {
            $this->month--;
            if ($this->month == 0) {
                $this->month = 12;
                $this->year--;
            }
        }

        // next button clicked
        if (isset($_POST['next'])) {
            $this->month++;
            if ($this->month == 13) {
                $this->month = 1;
                $this->year++;
            }
        }

        // today button clicked
        if (isset($_POST['today'])) {
            $this->month = $this->arDate('m');
            $this->year = $this->arDate('Y');
        }

        // store result in session, so if page refreshed same info will be shown.
        $_SESSION['HijriCalendarClassMonth'] = $this->month;
        $_SESSION['HijriCalendarClassYear'] = $this->year;

        // first day in the month
        $start = $this->arMktime(1, $this->month, $this->year);
        $this->time = $start;
        $day_of_week = $this->arDate('N', $start) . '<br />';
        // if the first day in the month if not the first day in the week, go back few days
        if ($day_of_week < 7) {
            $this->time = $start - ($this->arDate('N', $start) * 86400);
        }

        // some CSS needed
        $output = '<style type="text/css">
                .cc { display: block; width: 100%; height: 120px; min-width: 420px; position: relative; z-index: 1; }
                .cr { display: block; width: 100%; height: 120px; position: absolute; top: 0; left: 0; z-index: 1; }
                .vr { display: block; width: 100%; position: absolute; bottom: 10px; left: 0; z-index: 4; }
                .cr table { height: 121px; }
                .cr table.table.table-bordered tbody tr td { border-bottom: 0}
                .vr table tbody tr td { font-size: 11px}
                .calendar td { width: 14.28%; }
                .holiday { background: #eee; }
                .today { background: #FCF8E3; }
                .this-month { font-size: 28px; color: #333; }
                .other-month { font-size: 20px; color: #ccc; }
            </style>';
        // Calendar header
        $output .='<form action="" method="post" class="calendar">
                <div class="cc">
                <table class="table table-bordered" style="height: 100%">
                <tr>
                    <td colspan="7"><h4 class="pull-right">' . $this->arDate('M Yهـ', $this->arMktime(10, $this->month, $this->year)) . '</h4>
                        <div class="pull-left">
                            <input type="submit" name="prev" value="&lt;&lt;" />
                            <input type="submit" name="today" value="اليوم" />
                            <input type="submit" name="next" value="&gt;&gt;" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>الأحد</td>
                    <td>الاثنين</td>
                    <td>الثلاثاء</td>
                    <td>الأربعاء</td>
                    <td>الخميس</td>
                    <td class="holiday">الجمعة</td>
                    <td class="holiday">السبت</td>
                </tr></table></div>';
        // Calendar rows
        while ($this->arDate('m', $this->time) <= $this->month && $this->arDate('Y', $this->time) == $this->year || $this->arDate('Y',
                $this->time) < $this->year) {
            $output .= $this->printRow();
        }
        $output .= '</form>';
        return $output;
    }

    /**
     * Generate code for one row in the calendar (one week).
     * @return string html code for one row in the calendar.
     */
    private function printRow()
    {
        $colors = ['#A594C1', '#EAB57F', '#BAB28B', '#B7D28B', '#1D718D', '#6B6A6F', '#3A6CB3', '#817541', '#79A8E0', '#63802C', '#D28484', '#E15603'];
        $start = $reset = $this->time;
        $today = $this->arDate('Y/m/d');
        $output = '<div class="cc"><div class="cr"><table class="table table-bordered"><tr>';
        for ($i = 0; $i < 7; $i++) {
            $date = $this->arDate('Y/m/d', $this->time += 86400);
            $output .= '<td id="' . $this->arDate('Y-m-d',
                    $this->time) . '" class="' . ($i > 4 ? 'holiday ' : '') . ($this->arDate('m',
                    $this->time) == $this->month ? 'this-month' : 'other-month') . ' ' . ($date == $today ? 'today' : '') . '">' . $this->arNumbers((int)$this->arDate('d',
                    $this->time)) . '</td>' . "\n";
        }
        $output .= '</tr></table></div>';
        $end = $this->time;
        $output .= '<div class="vr">';
        $rows = 0;
        foreach ($this->events as $key => $row) {
            $row['start'] = $this->arMktime($row['start']);
            $row['end'] = $row['start'] + $row['duration'] * 86400;
            if (($row['start'] >= $start and $row['start'] < $end) or ($row['end'] >= $start and $row['end'] <= $end) or ($row['start'] < $start and $row['end'] > $end)) {
                $cols = 0;
                // limit number of events in the same week to prevent cover dates.
                if ($rows > 3) {
                    break;
                }
                $output .= '<table width="100%" height="15"><tr>';
                for ($i = 0; $i < 7; $i++) {
                    if ($start < $row['start']) {
                        $output .= '<td width="14.28%"></td>';
                        if ($cols > 0) {
                            $output .= '<td colspan="' . $cols . '" style="background: ' . $colors[$key] . '; width: ' . ($cols * 14.28) . '%;">' . $row['title'] . '</td>';
                            $cols = 0;
                        }
                    } elseif ($start >= $row['end']) {
                        if ($cols > 0) {
                            $output .= '<td colspan="' . $cols . '" style="background: ' . $colors[$key] . '; width: ' . ($cols * 14.28) . '%;">' . $row['title'] . '</td>';
                            $cols = 0;
                        }
                        $output .= '<td width="14.28%"></td>';
                    } else {
                        $cols++;
                    }
                    $start += 86400;
                }
                if ($cols > 0) {
                    $output .= '<td colspan="' . $cols . '" style="background: ' . $colors[$key] . '; width: ' . ($cols * 14.28) . '%;">' . $row['title'] . '</td>';
                }
                $output .= '</tr></table>';
                $rows++;
            }
            $start = $reset;
        }
        $output .= '</div></div>';
        return $output;
    }
}
